# BMI Calculator 

> **Note** <br>
> This android application was made for learning perposes. 

**What is Body Mass Index (BMI)?** <br>
Body Mass Index (BMI) is a measurement of a person’s weight with respect to his or her height. It is more of an indicator than a direct measurement of a person’s total body fat. 

BMI, more often than not, correlates with total body fat. This means that as the BMI score increases, so does a person’s total body fat. 

The WHO defines an adult who has a BMI between 25 and 29.9 as overweight - an adult who has a BMI of 30 or higher is considered obese - a BMI below 18.5 is considered underweight, and between 18.5 to 24.9 a healthy weight .

**BMI calculation** <br>
BMI in an individual is calculated by the use of a mathematical formula. It can also be estimated using tables in which one can match height in inches to weight in pounds to estimate BMI. There are convenient calculators available on internet sites that help calculate BMI as well.

The formula is - BMI = (Weight in kilograms) divided by (Height in metres squared)

A normal BMI score is one that falls between 18.5 and 24.9. This indicates that a person is within the normal weight range for his or her height. A BMI chart is used to categorize a person as underweight, normal, overweight, or obese.

| Body Mass Index (BMI) | Weight Status |
|-----------------------|---------------|
| Below 18.5            | Underweight   |
| 18.5 - 24.9           | Normal        |
| 25.0 - 29.9           | Overweight    |
| 30.0 plus             | Obese         |

### Methods 

* Splash Screen
* Alert Dialog with Checked Items
* Menu in AppBar
* BMI Calculation for cm and foot+inch 


## Screenshot 

![title](Capture1.PNG)

### Download APK
[![download_image](download_from_cloud_80.png)](https://gitlab.com/android-development3/bmi-calculator/-/blob/master/app/release/app-release.apk)

<!--
:checkered_flag: [Download APK](https://gitlab.com/android-development3/bmi-calculator/-/blob/master/app/release/app-release.apk)
-->

**Development By,** <br>
***:bd: Farhan Sadik***

### Sources

* [What is BMI?](https://www.news-medical.net/health/What-is-Body-Mass-Index-(BMI).aspx)
* Calculate your own body mass index [(Youtube Video)](https://www.youtube.com/watch?v=5RXRr8PKunk)
* [How to add radio button list in alert dialog?](https://www.tutorialspoint.com/how-to-add-radio-button-list-in-alert-dialog)
* [Alert Dialog](https://www.tutorialspoint.com/android/android_alert_dialoges.htm)
* [Android Table Layout](https://www.tutorialspoint.com/android/android_table_layout.htm) 
<!-- 
### Sources

**Tutorials**
* Calculate your own body mass index [(Youtube Video)](https://www.youtube.com/watch?v=5RXRr8PKunk)
* [How to add radio button list in alert dialog?](https://www.tutorialspoint.com/how-to-add-radio-button-list-in-alert-dialog)
* [Alert Dialog](https://www.tutorialspoint.com/android/android_alert_dialoges.htm)
* [Android Table Layout](https://www.tutorialspoint.com/android/android_table_layout.htm)

**Solutions**
* [Android Listview dialog doesn't disapear after choosing an item](https://stackoverflow.com/questions/15802399/android-listview-dialog-doesnt-disapear-after-choosing-an-item)
* [Android button background color](https://stackoverflow.com/questions/18070008/android-button-background-color)
```
android:backgroundTint="@android:color/holo_green_light"
```

* [How to disable EditText in Android](https://stackoverflow.com/questions/5879250/how-to-disable-edittext-in-android)
* [Disabled EditText can still be edited using on-screen keyboard](https://issuetracker.google.com/issues/36907909)
```
This works to disable the editText on startup (onCreate)

lonView = (EditText) findViewById(R.id.lonTextView);
lonView.setEnabled(false);
lonView.setFocusable(false);

But then this code, while it does ececute on a button click, does not restore the
editText to enabled.... is this a bug?

private final Button.OnClickListener b2Listener = new   Button.OnClickListener()

public void onClick(View v) {
    lonView.setEnabled(true);
    lonView.setFocusable(true);
    lonView.requestFocus();
    lonView.invalidate();
    lonView.setTextColor(Color.GREEN);
}
```

* [How to change visibility of layout programmatically](https://stackoverflow.com/questions/3465841/how-to-change-visibility-of-layout-programmatically)
```    
TextView view = (TextView) findViewById(R.id.textView);
view.setText("Add your text here");
view.setVisibility(View.VISIBLE)
```

* [How to save state and check default of radio button in alert dialog box(android)?](https://stackoverflow.com/questions/25433736/how-to-save-state-and-check-default-of-radio-button-in-alert-dialog-boxandroid)
```
public int selectedElement=-1; //global variable to store state
The -1 here is the default selected item index (-1 means do not select any default). use this parameter to set the default selected.
```
* [Android Programmatically setting button text](https://stackoverflow.com/questions/11154898/android-programmatically-setting-button-text)
```
Button mButton = (Button)your_view_object.findViewById(R.id.contact);
mButton.setText("number");
```
* [How to display calculations result in a hinted TextView in Android Studio/Java?](https://stackoverflow.com/questions/39048695/how-to-display-calculations-result-in-a-hinted-textview-in-android-studio-java)
```
TextView output = (TextView) findViewById(R.id.output);
output.setText(euroamount.toString());
```
* [writing some characters like '<' in an xml file](https://stackoverflow.com/questions/3166951/writing-some-characters-like-in-an-xml-file/3166967#3166967)
```
	&lt; for <
	&gt; for >
	&amp; for &
```
* [Android single button multiple functions](https://stackoverflow.com/questions/19433315/android-single-button-multiple-functions)
```
button = (Button) findViewById(R.id.button1);

button.setOnClickListener(new OnClickListener() {
    @Override
    public void onClick(View arg0) {
        String ButtonText = button.getText().toString();
        if ( ButtonText.equals("Save")){
            // code for save
        } else{
            // code for edit
        }
    }
});
```
* [How to set text color to a text view programmatically](https://stackoverflow.com/questions/8472349/how-to-set-text-color-to-a-text-view-programmatically)
```
Color.parseColor("#bdbdbd");
```
* [My app icon is not showing on some devices](https://stackoverflow.com/questions/55790124/my-app-icon-is-not-showing-on-some-devices)
```
File -> New -> Image Asset -> Launcher Icons (Adaptive and Legacy)
```
* [How to change the status bar color in Android?](https://stackoverflow.com/questions/22192291/how-to-change-the-status-bar-color-in-android)
```
Android 5.0 Lollipop introduced Material Design theme which automatically colors the status bar based on the colorPrimaryDark value of the theme.
```
-->
