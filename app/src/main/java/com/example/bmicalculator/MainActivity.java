package com.example.bmicalculator;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    Button heightButton, weightButton, calculateButton;
    EditText weightEditText, cmEditText, ft_and_inch_EditText;
    TextView resultTextView, statusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // weight
        weightButton = findViewById(R.id.weight_button);
        weightEditText = findViewById(R.id.weight_edittext);

        // height
        heightButton = findViewById(R.id.height_button); // button
        cmEditText = findViewById(R.id.height_cm_edittext); // cm
        ft_and_inch_EditText = findViewById(R.id.height_ft_and_inch_edittext); // ft
        //ft_and_inch_EditText.setEnabled(false); // by default

        // result
        resultTextView = findViewById(R.id.result); // this is a text view
        statusTextView = findViewById(R.id.status); // this is another text view
        calculateButton = findViewById(R.id.calculate_button);

        // calculation button on action
        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String w_kg = weightEditText.getText().toString(); // kg
                String h_cm = cmEditText.getText().toString(); // cm/foot
                String h_foot = ft_and_inch_EditText.getText().toString(); // in
                String ButtonText = heightButton.getText().toString(); // height button

                // calculating
                if (ButtonText.equals("CM")) {

                    cmCalculation(w_kg, h_cm);

                } else if (ButtonText.equals("FT+IN")) {

                    footCalculation(w_kg, h_cm, h_foot);

                } else {
                    Toast.makeText(getApplicationContext(), "null", Toast.LENGTH_LONG).show();
                }


            }
        });

    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item, menu);
        return true;
    }

    // on menu option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_bar_info) {
            startActivity(new Intent(getApplicationContext(), Info.class));
            return true;

        } else if (id == R.id.action_bar_about) {
            startActivity(new Intent(getApplicationContext(), About.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // height button on action
    public void height_button(View v) {
        AlertDialogFoot();
    }


    // alert dialog for height button
    private void AlertDialogFoot() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        //alertDialog.setTitle("AlertDialog");
        String[] items = {"CM", "FT+IN"};
        int checkedItem = -1;

        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // cm
                if (which == 0) {
                    ft_and_inch_EditText.setVisibility(View.GONE);
                    //heightButton.setText("FT+IN");
                    heightButton.setText("CM");
                    cmEditText.setHint("cm");
                    dialog.dismiss(); // to dismiss dialog

                // feet and inch
                } else if (which == 1) {
                    ft_and_inch_EditText.setEnabled(true);
                    cmEditText.setHint("ft");
                    ft_and_inch_EditText.setHint("in");
                    heightButton.setText("FT+IN");
                    ft_and_inch_EditText.setVisibility(View.VISIBLE);

                    dialog.dismiss(); // to dismiss dialog
                }
            }
        });

        AlertDialog alert = alertDialog.create();
        alert.show();
    }


    // cm calculation method
    public void cmCalculation(String weight, String userInput) {

        double valueOfCM = 0, result = 0;
        int valueOfKG =0;

        // converting
        if (!"".equals(weight)) { valueOfKG = Integer.parseInt(weight); };
        if (!"".equals(userInput)) { valueOfCM = Double.parseDouble(userInput); };

        // converting
        double m = valueOfCM / 100;
        double bmi = valueOfKG / (m * m);

        // decimal point
        NumberFormat number = new DecimalFormat("#0.00");
        String numFormat = number.format(bmi);
        if (!"".equals(numFormat)) {result = Double.parseDouble(numFormat); };

        // printing result
        resultTextView.setText("" + result);

        // printing category
        bmiStatus(result);
    }

    // foot and inch calculation method
    public void footCalculation(String weight, String userInput0, String userInput1) {

        double valueOfFoot = 0, valueOfInch = 0, result = 0;
        int valueOfKG = 0;

        // converting
        if (!"".equals(weight)) { valueOfKG = Integer.parseInt(weight); };
        if (!"".equals(userInput0)) { valueOfFoot = Integer.parseInt(userInput0); };
        if (!"".equals(userInput0)) { valueOfInch = Double.parseDouble(userInput1); };

        // converting
        double foot = 12 * valueOfFoot;
        double inch = foot + valueOfInch;
        double meter = 0.0254 * inch;

        // formula
        double bmi = valueOfKG / (meter * meter);

        // decimal point
        NumberFormat number = new DecimalFormat("#0.00");
        String numFormat = number.format(bmi);
        if (!"".equals(numFormat)) {result = Double.parseDouble(numFormat); };

        // printing result
        resultTextView.setText("" + result);

        // printing category
        bmiStatus(result);

    }

    // bmi category
    public void bmiStatus(double result) {

        String c1 = "Very Severely Underweight";
        String c2 = "Severely Underweight";
        String c3 = "Underweight";
        String c4 = "Normal";
        String c5 = "Overweight";
        String c6 = "Obese Class I";
        String c7 = "Obese Class II";
        String c8 = "Obese Class III";

        // Color.parseColor("#bdbdbd")


        if (result <= 16) {
            statusTextView.setText(c1 + " " + result);

        } else if (result <= 17) {
            statusTextView.setText(c2 + " " + result);

        } else if (result <= 18.5) {
            statusTextView.setText(c3 + " " + result);

        } else if (result <= 25) {
            statusTextView.setText(c4 + " " + result);

        } else if (result <= 30) {
            statusTextView.setText(c5 + " " + result);
            //statusTextView.setTextColor(Color.parseColor("#008000"));

        } else if (result <= 35) {
            statusTextView.setText(c6 + " " + result);

        } else if (result <= 40) {
            statusTextView.setText(c7 + " " + result);

        } else if (result >= 50) {
            statusTextView.setText(c8 + " " + result);

        } else {
            statusTextView.setText("no result found!");
        }

    }

}

